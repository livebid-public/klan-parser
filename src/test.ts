const getAlphabeticalIndex = (idx: number): string => {
  if (idx === 0) return "";

  const alphabet = "abcdefghijklmnopqrstuvwxyz";
  const result = getAlphabeticalIndex(Math.floor((idx - 1) / 26)) + alphabet[(idx - 1) % 26];

  return result;
};

getAlphabeticalIndex(0); // ?
getAlphabeticalIndex(1); // ?
getAlphabeticalIndex(26); // ?
getAlphabeticalIndex(27); // ?
