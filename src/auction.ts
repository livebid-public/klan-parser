import { LocalDate } from "@js-joda/core";
import { AuctionCar } from "./auctionCar";

export class Auction {
  private name: string;
  private date: LocalDate;
  private itemCount: number;
  private path: string;
  private carsOffset: number = 0;
  private cars: AuctionCar[];

  constructor(name: string, date: LocalDate, itemCount: number, path: string) {
    this.name = name;
    this.date = date;
    this.itemCount = itemCount;
    this.path = path;
  }

  public getName(): string {
    return this.name;
  }

  public getDate(): LocalDate {
    return this.date;
  }

  public getItemCount(): number {
    return this.itemCount;
  }

  public getPath(): string {
    return this.path;
  }

  public getCars(): AuctionCar[] {
    return this.cars;
  }

  public setCars(cars: AuctionCar[]): void {
    this.cars = cars;
  }

  public getCarsOffset(): number {
    return this.carsOffset;
  }

  public setCarsOffset(carsOffset: number): void {
    this.carsOffset = carsOffset;
    this.cars.forEach((car, index) => {
      car.setCarNumber(index + carsOffset);
    });
  }

  public presentAuction(): string {
    return `${this.name}  |  (Date: ${this.date}, #Items: ${this.itemCount})`;
  }

  public presentCars(): void {
    if (this.cars === null) return;
    this.cars.forEach((car) => car.presentCar(this.name));
  }

  public toString(): string {
    return `{ NAME: "${this.name}", DATE: "${this.date}", ITEM_COUNT: ${this.itemCount}, PATH: "${this.path}" }`;
  }
}
