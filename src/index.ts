import { KlanApplication } from "./klanApplication";

const app = new KlanApplication();
app
  .run()
  .then(() => {
    process.exit(0);
  })
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
