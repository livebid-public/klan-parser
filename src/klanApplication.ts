import { DateTimeFormatter, LocalDate } from "@js-joda/core";
import * as cheerio from "cheerio";
import * as fs from "fs";
import fetch from "node-fetch";
import * as path from "path";
import * as readline from "readline";
import * as XLSX from "xlsx";
import { Auction } from "./auction";
import { AuctionCar } from "./auctionCar";
import { getAlphabeticalIndex } from "./utils";

const $$ = cheerio.load("");

export class KlanApplication {
  private static readonly DATE_FORMATTER = DateTimeFormatter.ofPattern("d.M.yyyy");
  private static readonly ORDER_SEQUENCE_REGEX = /^[0-9]+(,[0-9]+)*$/;
  private static readonly CAR_DETAIL_REGEX = /.*:([0-9]+)/;

  private async requestUrlFromUser(): Promise<URL> {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    const question = (query: string): Promise<string> => new Promise((resolve) => rl.question(query, resolve));

    while (true) {
      const line = await question(
        "Type in the URL of the auction (protocol://host/path e.g. http://www.klankolin.cz/katalog/2020-11):\n"
      );
      try {
        rl.close();
        return new URL(line);
      } catch (e) {
        console.log("ERROR: incorrect URL, try again..");
      }
    }
  }

  private isNotAuctionTableHeader(tableRow: cheerio.Element): boolean {
    return $$(tableRow).children().length !== 4;
  }

  private isNotCarTableHeader(tableRow: cheerio.Element): boolean {
    return $$(tableRow).children().length !== 6;
  }

  private tableRowToAuction(tableRow: cheerio.Element): Auction {
    const rowCells = $$(tableRow).children();
    if (rowCells.length !== 5) {
      console.log("Debug: skipping row with != 5 number of cells");
    }
    this.checkTableRowCells(rowCells);
    const cellWithPath = rowCells.eq(0);
    const path = cellWithPath.find("a").attr("href") || "";
    return new Auction(
      rowCells.eq(1).text(),
      LocalDate.parse(rowCells.eq(2).text(), KlanApplication.DATE_FORMATTER),
      parseInt(rowCells.eq(3).text(), 10),
      path
    );
  }

  private checkTableRowCells(rowCells: cheerio.Cheerio<any>): void {
    rowCells.each((_, rowCell) => {
      if (rowCell.tagName !== "td") {
        console.log("Error: expecting only <td> elements in the table data rows.");
      }
    });
  }

  private tableRowToCar(tableRow: cheerio.Element): AuctionCar {
    const rowCells = $$(tableRow).children();
    if (rowCells.length !== 7) {
      console.log("Error: unexpected format of Table row with a car information");
      throw new Error("");
    }
    this.checkTableRowCells(rowCells);
    const car = new AuctionCar();
    const tagWithPath = rowCells.eq(2).find("a");
    car.setPath(tagWithPath.attr("href")!);
    car.setCarName(tagWithPath.text());
    const odometer = rowCells.eq(3).text();
    car.setOdometer(odometer.toLowerCase().includes("nezji") ? -1 : parseInt(odometer.replace(/\s/g, ""), 10));
    const firstRegistration = rowCells.eq(4).text();
    car.setFirstRegistration(firstRegistration ? parseInt(firstRegistration, 10) : -1);
    const startPrice = rowCells.eq(5).text();
    car.setStartPrice(
      startPrice.toLowerCase().includes("nestanoven")
        ? -1
        : parseInt(startPrice.replace(/\s/g, "").split("&")[0].split("K")[0].replace(/[^\d]/g, ""), 10)
    );
    return car;
  }

  private async fetchAuctionList(auctionUrl: URL): Promise<Auction[]> {
    const response = await fetch(auctionUrl.toString());
    const html = await response.text();
    const $ = cheerio.load(html);
    const tables = $("table.HWTAB > tbody");
    if (tables.length !== 1) throw new Error("Error: there should be exactly one table with auctions!");
    const tableRows = tables.children();
    if (tableRows.length < 2) throw new Error("Error: no auction in the table!");
    return tableRows
      .filter((_, row) => this.isNotAuctionTableHeader(row))
      .map((_, row) => this.tableRowToAuction(row))
      .get();
  }

  private validateOrderSequence(sequence: string): boolean {
    if (!KlanApplication.ORDER_SEQUENCE_REGEX.test(sequence)) {
      console.log("Error: expected numbers divided by commas ( regex: [0-9]+(,[0-9]+)* )");
      return false;
    }
    return true;
  }

  private parseOrderSequence(sequence: string, maxValue: number): Set<number> | null {
    const numbers = new Set<number>();
    const tokenizer = sequence.split(",");
    for (const token of tokenizer) {
      const number = parseInt(token, 10);
      if (number > maxValue) {
        console.log(`Error: the numbers cannot be bigger than last auction number (${maxValue}).`);
        return null;
      }
      if (number === 0) {
        console.log("Error: the numbers cannot be equal to 0.");
        return null;
      }
      numbers.add(number);
    }
    return numbers;
  }

  private async askUserForAuctionOrder(auctions: Auction[]): Promise<Auction[]> {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    const question = (query: string): Promise<string> => new Promise((resolve) => rl.question(query, resolve));

    while (true) {
      console.log("\n=====  List of auctions =====\n");
      auctions.forEach((auction, idx) => console.log(`${idx + 1}. ${auction.getName()}`));
      console.log();

      const orderSequence = await question('Type in a sequence of numbers representing the order of auctions (e.g. "1,0,2"):\n');
      if (!this.validateOrderSequence(orderSequence)) continue;

      const orderNumbers = this.parseOrderSequence(orderSequence, auctions.length);
      if (!orderNumbers) continue;
      if (orderNumbers.size !== auctions.length) {
        console.log("Error: amount of numbers must be equal to number of auctions.");
        continue;
      }
      rl.close();
      return Array.from(orderNumbers).map((orderNumber) => auctions[orderNumber - 1]);
    }
  }

  private async fetchCarsForAuction(auctionUrl: URL, auction: Auction): Promise<void> {
    const uri = `${auctionUrl.protocol}//${auctionUrl.host}/${auction.getPath()}`;
    const response = await fetch(uri);
    const html = await response.text();
    const $ = cheerio.load(html);
    const tableRows = $("table.HWTAB > tbody > tr");
    if (tableRows.length === 0) throw new Error(`Error: no cars in table found for auction "${auction.getName()}"!`);
    auction.setCars(
      tableRows
        .filter((_, row) => this.isNotCarTableHeader(row))
        .map((_, row) => this.tableRowToCar(row))
        .get()
    );
  }

  private async fetchCarDetails(auctionUrl: URL, auction: Auction): Promise<void> {
    for (const car of auction.getCars()) {
      const uri = `${auctionUrl.protocol}//${auctionUrl.host}/${car.getPath()}`;
      const response = await fetch(uri);
      const html = await response.text();
      const $ = cheerio.load(html);

      const carImages = $("div#MAIN > div.galery > a");
      if (carImages.length < 1) {
        console.log(`INFO: did not find any tags with car images for "${car.getCarName()}"`);
      } else {
        const imagesPaths = carImages.map((_, img) => $(img).attr("href")!).get();
        car.setCarImagePaths(imagesPaths);
      }

      const carDetails = $("div#MAIN > div.LEFTCOL > div");
      if (carDetails.length < 1) {
        console.log(`Error: did not find any tags with car details for "${car.getCarName()}"`);
        return;
      }

      carDetails.each((i, detail) => {
        const detailText = cheerio.load(detail).text().toLowerCase().replace(/\s/g, "");
        let detailMatcher: RegExpExecArray | null;

        if (detailText.startsWith("zdvihov")) {
          detailMatcher = KlanApplication.CAR_DETAIL_REGEX.exec(detailText);
          if (detailMatcher) {
            car.setEngineVolume(parseInt(detailMatcher[1]));
          } else {
            console.log(`Error: could not parse car detail "Zdvihovy objem" for "${car.getCarName()}"`);
            car.setEngineVolume(-1);
          }
        } else if (detailText.startsWith("výkon")) {
          detailMatcher = KlanApplication.CAR_DETAIL_REGEX.exec(detailText);
          if (detailMatcher) {
            car.setEnginePower(parseInt(detailMatcher[1]));
          } else {
            console.log(`Error: could not parse car detail "Vykon (kW)" for "${car.getCarName()}"`);
            car.setEnginePower(-1);
          }
        } else {
          const parts = detailText.split(":");
          if (detailText.startsWith("palivo") && parts.length === 2) {
            car.setFuel(parts[1]);
          } else if (detailText.startsWith("klánid") && parts.length === 2) {
            car.setKlanId(parts[1]);
          } else if (detailText.startsWith("doklady") && parts.length === 2) {
            if (parts[1].includes("tp")) {
              car.setTP(true);
            }
            if (parts[1].includes("orv")) {
              car.setORV(true);
            }
          } else if (detailText.startsWith("poznámka")) {
            car.setNote(cheerio.load(detail).text().replace("Poznámka:", "").trim());
          } else if (detailText.startsWith("odpočet") && parts.length === 2) {
            car.setDPH(parts[1].includes("ano"));
          }
        }
      });
    }
  }

  private exportToExcel(dirName: string, auctions: Auction[]): void {
    const workbook = XLSX.utils.book_new();
    const wsData: any[][] = [
      [
        "lotN",
        "Nazev drazby",
        "Nazev auta",
        "Prvni registrace",
        "Objem",
        "Vykon",
        "Tachometr",
        "Palivo",
        "KlanID",
        "TP",
        "ORV",
        "DPH",
        "Poznamka",
        "Vyvolavaci cena",
      ],
    ];
    auctions.forEach((auction) => {
      auction.getCars().forEach((car) => {
        wsData.push([
          car.getCarNumber() || "",
          auction.getName() || "",
          car.getCarName() || "",
          car.getFirstRegistration() || "",
          car.getEngineVolume() || "",
          car.getEnginePower() || "",
          car.getOdometer() || "",
          car.getFuel() || "",
          car.getKlanId() || "",
          car.getTP() || "",
          car.getORV() || "",
          car.getDPH() || "",
          car.getNote() || "",
          car.getStartPrice() || "",
        ]);
      });
    });
    const worksheet = XLSX.utils.aoa_to_sheet(wsData);
    XLSX.utils.book_append_sheet(workbook, worksheet, "Sheet 1");
    XLSX.writeFile(workbook, path.join(dirName, "table.xlsx"));
    console.log(`INFO: writing to excel sheet on path "./${dirName}/table.xlsx"`);
  }

  private async fetchImages(dirName: string, auctionUrl: URL, auctions: Auction[]): Promise<void> {
    for (const auction of auctions) {
      for (const car of auction.getCars()) {
        if (!car.getCarImagePaths()) continue;
        for (let imageIdx = 0; imageIdx < Math.min(car.getCarImagePaths().length, 27); imageIdx++) {
          const imagePath = car.getCarImagePaths()[imageIdx];
          const imageUrl = `${auctionUrl.protocol}//${auctionUrl.host}/${imagePath}`;
          const suffix = getAlphabeticalIndex(imageIdx);
          const imageOutputPath = path.join(dirName, `${car.getCarNumber()}${suffix}.jpg`);
          console.log(`Fetching image: "${imageUrl}" -> Saving to: "${imageOutputPath}"`);
          try {
            const response = await fetch(imageUrl);
            const buffer = await response.buffer();
            fs.writeFileSync(imageOutputPath, buffer);
          } catch (e) {
            console.log(`Unable to retrieve Image with path "${imagePath}" for car "${car.getCarName()}"`);
          }
        }
      }
    }
  }

  async run() {
    console.log("\n===== KLAN PARSER =====\n");
    const auctionUrl = await this.requestUrlFromUser();
    const urlPathSegments = auctionUrl.pathname.split("/");
    let outputDirectoryName =
      urlPathSegments.length < 1 || urlPathSegments[urlPathSegments.length - 1].trim() === ""
        ? "klan_parser_outputs"
        : urlPathSegments[urlPathSegments.length - 1];
    outputDirectoryName = `out/${outputDirectoryName}`;
    console.log(`INFO: output directory "./${outputDirectoryName}"`);

    try {
      fs.mkdirSync(outputDirectoryName, {
        recursive: true,
      });
    } catch (e) {}

    let auctions = await this.fetchAuctionList(auctionUrl);
    auctions = await this.askUserForAuctionOrder(auctions);
    console.log("\n=====  Reordered list of auctions =====\n");
    auctions.forEach((auction, idx) => console.log(`${idx + 1}. ${auction.getName()}`));
    console.log();

    for (const auction of auctions) {
      await this.fetchCarsForAuction(auctionUrl, auction);
      await this.fetchCarDetails(auctionUrl, auction);
    }

    let carCounter = 1;
    console.log("\n=====  Car list =====\n");
    for (const auction of auctions) {
      auction.setCarsOffset(carCounter);
      auction.presentCars();
      carCounter += auction.getCars().length;
    }
    console.log();

    this.exportToExcel(outputDirectoryName, auctions);

    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    const question = (query: string): Promise<string> => new Promise((resolve) => rl.question(query, resolve));

    const fetchImages = await question('Should car images be fetched now (type "yes")?\n');
    rl.close();

    if (fetchImages !== "yes") {
      console.log("INFO: image fetching aborted.");
      return;
    }

    await this.fetchImages(outputDirectoryName, auctionUrl, auctions);
    console.log("\n===== DONE =====\n");
  }
}
