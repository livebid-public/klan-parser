export class AuctionCar {
  private carName: string;
  private firstRegistration: number = -2;
  private engineVolume: number = -2;
  private enginePower: number = -2;
  private odometer: number = -2;
  private fuel: string;
  private klanId: string;
  private TP: boolean = false;
  private ORV: boolean = false;
  private DPH: boolean = false;
  private note: string;
  private startPrice: number = -2;
  private path: string;
  private carNumber: number;
  private carImagePaths: string[];

  private pB(b: boolean): string {
    return b ? "ano" : "ne";
  }

  public presentCar(auctionName: string): void {
    console.log(
      `${this.carNumber}. ${auctionName}, ${this.carName}, ${this.firstRegistration}, ${this.engineVolume}, ${
        this.enginePower
      }, ${this.odometer}, ${this.fuel}, ${this.klanId}, ${this.pB(this.TP)}, ${this.pB(this.ORV)}, ${this.pB(this.DPH)}, ${
        this.note
      }, ${this.startPrice}`
    );
  }

  public getCarName(): string {
    return this.carName;
  }

  public setCarName(carName: string): void {
    this.carName = carName;
  }

  public getFirstRegistration(): number {
    return Math.max(this.firstRegistration, 0);
  }

  public setFirstRegistration(firstRegistration: number): void {
    this.firstRegistration = firstRegistration;
  }

  public getEngineVolume(): number {
    return Math.max(this.engineVolume, 0);
  }

  public setEngineVolume(engineVolume: number): void {
    this.engineVolume = engineVolume;
  }

  public getEnginePower(): number {
    return Math.max(this.enginePower, 0);
  }

  public setEnginePower(enginePower: number): void {
    this.enginePower = enginePower;
  }

  public getOdometer(): number {
    return Math.max(this.odometer, 0);
  }

  public setOdometer(odometer: number): void {
    this.odometer = odometer;
  }

  public getFuel(): string {
    return this.fuel;
  }

  public setFuel(fuel: string): void {
    this.fuel = fuel;
  }

  public getKlanId(): string {
    return this.klanId;
  }

  public setKlanId(klanId: string): void {
    this.klanId = klanId;
  }

  public getTP(): string {
    return this.pB(this.TP);
  }

  public getORV(): string {
    return this.pB(this.ORV);
  }

  public getDPH(): string {
    return this.pB(this.DPH);
  }

  public setTP(TP: boolean): void {
    this.TP = TP;
  }

  public setORV(ORV: boolean): void {
    this.ORV = ORV;
  }

  public setDPH(DPH: boolean): void {
    this.DPH = DPH;
  }

  public getNote(): string {
    return this.note;
  }

  public setNote(note: string): void {
    this.note = note;
  }

  public getPath(): string {
    return this.path;
  }

  public setPath(path: string): void {
    this.path = path;
  }

  public getStartPrice(): number {
    return Math.max(this.startPrice, 0);
  }

  public setStartPrice(startPrice: number): void {
    this.startPrice = startPrice;
  }

  public getCarNumber(): number {
    return Math.max(this.carNumber, 0);
  }

  public setCarNumber(carNumber: number): void {
    this.carNumber = carNumber;
  }

  public getCarImagePaths(): string[] {
    return this.carImagePaths;
  }

  public setCarImagePaths(carImagePaths: string[]): void {
    this.carImagePaths = carImagePaths;
  }
}
