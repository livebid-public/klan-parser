import { getAlphabeticalIndex } from "./utils";
import { expect, test } from "bun:test";

describe("getAlphabeticalIndex", () => {
  it("should return the correct alphabetical index", () => {
    expect(getAlphabeticalIndex(0)).toBe("");
    expect(getAlphabeticalIndex(1)).toBe("a");
    expect(getAlphabeticalIndex(26)).toBe("z");
    expect(getAlphabeticalIndex(27)).toBe("aa");
  });
});
