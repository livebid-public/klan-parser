# Klán parser

Simple parser for the [klán](https://www.klankolin.cz/katalog).

## Instalation

First of all you need to have installed [bun](https://bun.sh/). Then you can install the klán parser with the following command:

```bash
bun install
```

## Usage

You can use the klán parser with the following command:

```bash
bun start
```
